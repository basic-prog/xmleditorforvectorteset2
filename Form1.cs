﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.Xml;
using Microsoft.Win32;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        XmlDocument XmlDoc = new XmlDocument();
        XmlNode BufferXmlNode = null;
        string FilePath = "";
        XmlNode SelectedXmlNode = null;
        TreeNode SelectedTreeNode = null;
        int[] ColumnWidth = new int[3];

        public Form1()
        {
            InitializeComponent();

            ///////////////////////////////////////////////////////////////////////////
            //FilePath = "C:\\Users\\kimkw2\\Desktop\\WindowsFormsApplication1\\Coding.xml";
            //XmlDoc.Load(FilePath);
            ColumnWidth[0] = ColumnWidth[1] = ColumnWidth[2] = 200;
            
            //updateTreeview();
        }

        private bool licCheck()
        {
            RegistryKey rkeyHidden = Registry.CurrentUser.OpenSubKey(@"0VXmlEditor");
            RegistryKey rkey = Registry.CurrentUser.OpenSubKey(@"VXmlEditor");

            if (rkeyHidden == null && rkey == null)
            {
                // 처음 실행
                rkeyHidden = Registry.CurrentUser.CreateSubKey(@"0VXmlEditor");
                rkey = Registry.CurrentUser.CreateSubKey(@"VXmlEditor");
            }
            else if (rkeyHidden != null && rkey != null)
            {
                ;
            }
            else
            {
                // 레지스트리 조작
                MessageBox.Show("License Expired. Please send e-mail to intheegg2@gmail.com for license.");
                return false;
            }

            string currentDate = DateTime.Now.ToString("yyyyMMdd");
            RegistryKey rKeyRoot = Registry.CurrentUser.OpenSubKey(@"VXmlEditor");
            string[] strSubkeys = rKeyRoot.GetSubKeyNames();
            int nSubKey = rKeyRoot.SubKeyCount;

            if (nSubKey == 0)
            {
                Registry.CurrentUser.CreateSubKey(@"VXmlEditor\" + currentDate);
                //for (int i = 30; i >= 1; i--)
                //{
                //    DateTime date = DateTime.Now.AddDays(-i);
                //    Registry.CurrentUser.CreateSubKey(@"VXmlEditor\" + date.ToString("yyyyMMdd"));
                //}
            }
            else if (nSubKey > 30)
            {   
                MessageBox.Show("License Expired. Please send e-mail to intheegg2@gmail.com for license.");
                return false;
            }
            else if (nSubKey > 0 && strSubkeys[nSubKey - 1] != currentDate)
            {
                Registry.CurrentUser.CreateSubKey(@"VXmlEditor\" + currentDate);
            }

            return true;
        }

        private void updateTreeview()
        {
            treeView1.Nodes.Clear();
            treeView1.Nodes.Add(new TreeNode(XmlDoc.DocumentElement.Name));

            TreeNode treeNode = treeView1.Nodes[0];
            AddTreeNode(XmlDoc.DocumentElement, treeNode);

            //treeView1.ExpandAll();
        }

        private void AddTreeNode(XmlNode xmlElem, TreeNode treeNode)
        {
            //TreeNode node = new TreeNode("Node1");
            //treeView.Nodes.Add(node);
            //XmlElement xmlElem = xmlDoc.DocumentElement;
            XmlNodeList xmlNodeList;
            XmlNode xmlNode;
            
            int nCnt = 0;

            if (xmlElem.HasChildNodes)
            {
                xmlNodeList = xmlElem.ChildNodes;
                for(int i=0; i < xmlNodeList.Count; i++)
                {
                    xmlNode = xmlElem.ChildNodes[i];

                    if (xmlNode.Name == "testmodule" ||
                        xmlNode.Name == "testgroup" ||
                        xmlNode.Name == "testcase")
                    {
                        XmlAttribute attr = null;
                        if (xmlNode.Attributes != null)
                            attr = xmlNode.Attributes["title"];

                        if (attr != null)
                            treeNode.Nodes.Add(new TreeNode(attr.Value));
                        else
                            treeNode.Nodes.Add(new TreeNode(xmlNode.Name));

                        

                        TreeNode tNode = treeNode.Nodes[nCnt++];
                        AddTreeNode(xmlNode, tNode);
                    }
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //treeView1.Nodes[0].Nodes[0].Nodes[0].Nodes[1].ExpandAll();
            treeView1.SelectedNode = treeView1.Nodes[0].Nodes[0].Nodes[0].Nodes[1];
            treeView1.SelectedNode.ExpandAll();
            
            treeView1.Focus();
        }

        private TreeNode treeNodeFromXmlNode(XmlNode xmlnode)
        {
            XmlNode curNode = xmlnode;
            XmlNode parent = xmlnode.ParentNode;

            List<int> list = new List<int>();
            while (parent != null)
            {
                XmlNodeList nodeList = parent.ChildNodes;
                int index = 0;

                for (int i=0; i<nodeList.Count; i++)
                {
                    if (nodeList[i].Name == "testmodule" ||
                        nodeList[i].Name == "testgroup" ||
                        nodeList[i].Name == "testcase")
                    {
                        if (nodeList[i] == curNode)
                        {
                            list.Add(index);
                            curNode = parent;
                            parent = parent.ParentNode;
                            break;
                        }
                        else
                        {
                            index++;
                        }
                    }   
                }
            }

            TreeNode treeNode = treeView1.Nodes[0];
            for(int i=list.Count-2; i>=0; i--)
            {
                treeNode = treeNode.Nodes[list[i]];
            }

            return treeNode;
        }

        private XmlNode xmlNodeFromTreeNode(TreeNode treeNode, XmlDocument xmlDoc)
        {
            TreeNode selectedTreeNode = treeNode;
            TreeNode tmpTreeNode = treeNode;
            List<int> list = new List<int>();
            for (int i = 0; i <= selectedTreeNode.Level; i++)
            {
                list.Add(tmpTreeNode.Index);
                tmpTreeNode = tmpTreeNode.Parent;
            }
            
            XmlNodeList nodeList = xmlDoc.ChildNodes;
            XmlNode xNode = null;
            for (int i=list.Count-1; i>=0; i--)
            {
                int index = 0;
                for(int j=0; j<nodeList.Count; j++)
                {
                    if( nodeList[j].Name == "testmodule" ||
                        nodeList[j].Name == "testgroup" ||
                        nodeList[j].Name == "testcase")
                    {
                        if(index == list[i])
                        {
                            xNode = nodeList[j];
                            nodeList = nodeList[j].ChildNodes;
                            break;
                        }
                        else
                        {
                            index++;
                        }
                    }
                }
            }

            return xNode;
        }

        private void treeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {   
            SelectedXmlNode = xmlNodeFromTreeNode(treeView1.SelectedNode, XmlDoc);
            SelectedTreeNode = treeView1.SelectedNode;

            if (SelectedXmlNode != null && SelectedXmlNode.Name == "testcase")
            {
                XmlNode nodeTestFunc = null;
                XmlNodeList paramlist = null;

                for (int i=0; i<SelectedXmlNode.ChildNodes.Count; i++)
                {
                    if( SelectedXmlNode.ChildNodes[i].Name == "capltestfunction" ||
                        SelectedXmlNode.ChildNodes[i].Name == "nettestfunction" )
                    {
                        nodeTestFunc = SelectedXmlNode.ChildNodes[i];
                        paramlist = nodeTestFunc.ChildNodes;
                    }
                }
                
                string strParam = "";
                for(int i=0; i<paramlist.Count; i++)
                {
                    XmlNode node = paramlist[i];
                    strParam += node.Attributes["type"].Value + ' ';
                    strParam += node.Attributes["name"].Value + "=";
                    strParam += node.InnerText;

                    if(i!= paramlist.Count-1)
                        strParam += "\r\n";
                }
                
                DataTable table = new DataTable();
                table.Columns.Add("title", typeof(string));
                table.Columns.Add("name", typeof(string));
                table.Columns.Add("param", typeof(string));
                table.Rows.Add(SelectedXmlNode.Attributes["title"].Value, nodeTestFunc.Attributes["name"].Value, strParam);
                dataGridView1.DataSource = table;

                dataGridView1.Columns[2].DefaultCellStyle.WrapMode = DataGridViewTriState.True;
                commonDataGridViewSetting();
            }
            else if (SelectedXmlNode != null && (SelectedXmlNode.Name == "testgroup" || SelectedXmlNode.Name == "testgmodule"))
            {
                DataTable table = new DataTable();
                table.Columns.Add("title", typeof(string));
                table.Rows.Add(SelectedXmlNode.Attributes["title"].Value);
                dataGridView1.DataSource = table;

                commonDataGridViewSetting();
            }
            else
            {
                dataGridView1.DataSource = null;
            }
        }

        private void commonDataGridViewSetting()
        {
            
            //dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
            //dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.None;
            for (int i=0; i<dataGridView1.Columns.Count; i++)
            {
                dataGridView1.Columns[i].Width = ColumnWidth[i];
            }
            
            //dataGridView1.Rows[0].DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            dataGridView1.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            //dataGridView1.AutoSize = true;
            dataGridView1.AllowUserToAddRows = false;
        }

        private void treeView1_MouseUp(object sender, MouseEventArgs e)
        {
            //// Make sure this is the right button.
            //if (e.Button != MouseButtons.Right) return;

            //// Select this node.
            //TreeNode node_here = treeView1.GetNodeAt(e.X, e.Y);
            //treeView1.SelectedNode = node_here;

            //// See if we got a node.
            //if (node_here == null) return;

            //ContextMenu m = new ContextMenu();
            //MenuItem m1 = new MenuItem();
            //MenuItem m2 = new MenuItem();
            //MenuItem m3 = new MenuItem();
            //m1.Text = "Cut";
            //m2.Text = "Copy";
            //m3.Text = "Paste";
            //m1.Click += (senders, es) =>
            //{ };
            //m2.Click += (senders, es) =>
            //{ };
            //m3.Click += (senders, es) =>
            //{ };

            //m.MenuItems.Add(m1);
            //m.MenuItems.Add(m2);
            //m.MenuItems.Add(m3);
            //m.Show(treeView1, e.Location);
        }



        private void expandAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            treeView1.ExpandAll();
        }

        private void collapseAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            treeView1.CollapseAll();
        }

        private void delNode()
        {
            if (!treeView1.Focused)
                return;

            if (treeView1.SelectedNode == null || treeView1.SelectedNode.Text == "testmodule")
                return;

            XmlNode node = xmlNodeFromTreeNode(treeView1.SelectedNode, XmlDoc);
            XmlNode parent = node.ParentNode;
            parent.RemoveChild(node);
            
            TreeNode nextTreeNode = treeView1.SelectedNode.NextNode;
            TreeNode prevTreeNode = treeView1.SelectedNode.PrevNode;
            treeView1.SelectedNode.Remove();
            if (nextTreeNode != null)
                treeView1.SelectedNode = nextTreeNode;
            else if(prevTreeNode != null)
                treeView1.SelectedNode = prevTreeNode;
        }

        private XmlNode createTestGroupNode()
        {
            XmlAttribute titleAttr = XmlDoc.CreateAttribute("title");
            titleAttr.Value = "newTestGroup";
            XmlNode newTestGroupNode = XmlDoc.CreateNode("element", "testgroup", null);
            newTestGroupNode.Attributes.Append(titleAttr);

            return newTestGroupNode;
        }

        private void newTestGroupNodeSub()
        {
            if (!treeView1.Focused)
                return;

            if (treeView1.SelectedNode == null)
                return;

            XmlNode node = xmlNodeFromTreeNode(treeView1.SelectedNode, XmlDoc);

            if (node == null || node.Name == "testcase")
                return;

            XmlNode newNode = createTestGroupNode();

            node.AppendChild(newNode);
            updateTreeview();
            treeView1.SelectedNode = treeNodeFromXmlNode(newNode);
        }

        private void newTestGroupNode()
        {
            if (!treeView1.Focused)
                return;

            if (treeView1.SelectedNode == null || treeView1.SelectedNode.Text == "testmodule")
                return;

            XmlNode newNode = createTestGroupNode();

            XmlNode node = xmlNodeFromTreeNode(treeView1.SelectedNode, XmlDoc);
            node.ParentNode.AppendChild(newNode);
            updateTreeview();
            treeView1.SelectedNode = treeNodeFromXmlNode(newNode);
        }

        private XmlAttribute makeXmlAttr(string strAttr, string strValue)
        {
            XmlAttribute attr = XmlDoc.CreateAttribute(strAttr);
            attr.Value = strValue;
            return attr;
        }

        private XmlNode createTestCaseNode(string strTestFuncType)
        {
            string strTestFunc = "capltestfunction";
            string strTestFuncParam = "caplparam";
            if (netToolStripMenuItem.Checked)
            {
                strTestFunc = "nettestfunction";
                strTestFuncParam = "netparam";
            }


            XmlNode testCaseNode = XmlDoc.CreateNode("element", "testcase", null);
            testCaseNode.Attributes.Append(makeXmlAttr("title", "title"));
            testCaseNode.Attributes.Append(makeXmlAttr("ident", "ident"));

            XmlNode testFuncNode = XmlDoc.CreateNode("element", strTestFunc, null);
            testFuncNode.Attributes.Append(makeXmlAttr("title", "title"));
            testFuncNode.Attributes.Append(makeXmlAttr("name", "name"));

            XmlNode testFuncParamNode = XmlDoc.CreateNode("element", strTestFuncParam, null);
            testFuncParamNode.Attributes.Append(makeXmlAttr("type", "type"));
            testFuncParamNode.Attributes.Append(makeXmlAttr("name", "name"));
            testFuncParamNode.InnerText = "0";

            testFuncNode.AppendChild(testFuncParamNode);
            testCaseNode.AppendChild(testFuncNode);

            return testCaseNode;
        }

        private void newTestCaseNodeSub()
        {
            if (!treeView1.Focused)
                return;

            if (treeView1.SelectedNode == null)
                return;

            XmlNode node = xmlNodeFromTreeNode(treeView1.SelectedNode, XmlDoc);

            if (node.Name == "testcase")
                return;

            XmlNode newNode = createTestCaseNode(node.Name);
            
            node.AppendChild(newNode);
            updateTreeview();

            treeView1.SelectedNode = treeNodeFromXmlNode(newNode);
        }


        private void newTestCaseNode()
        {
            if (!treeView1.Focused)
                return;

            if (treeView1.SelectedNode == null || treeView1.SelectedNode.Text == "testmodule")
                return;

            XmlNode node = xmlNodeFromTreeNode(treeView1.SelectedNode, XmlDoc);
            XmlNode newNode = createTestCaseNode(node.Name);
            node.ParentNode.InsertAfter(newNode, node);
            updateTreeview();

            treeView1.SelectedNode = treeNodeFromXmlNode(newNode);
        }


        private void copyNode()
        {
            if (!treeView1.Focused)
                return;
            
            if (treeView1.SelectedNode == null || treeView1.SelectedNode.Text == "testmodule")
                return;

            XmlNode xmlnode = xmlNodeFromTreeNode(treeView1.SelectedNode, XmlDoc);
            BufferXmlNode = xmlnode.Clone();
        }
        private void cutNode()
        {
            if (!treeView1.Focused)
                return;

            if (treeView1.SelectedNode == null || treeView1.SelectedNode.Text == "testmodule")
                return;

            TreeNode nextNode = treeView1.SelectedNode.NextNode;
            TreeNode prevtNode = treeView1.SelectedNode.PrevNode;

            BufferXmlNode = xmlNodeFromTreeNode(treeView1.SelectedNode, XmlDoc);
            BufferXmlNode.ParentNode.RemoveChild(BufferXmlNode);
            
            treeView1.SelectedNode.Remove();
            if (nextNode != null)
                treeView1.SelectedNode = nextNode;
            else if(prevtNode != null)
                treeView1.SelectedNode = prevtNode;
        }
        private void pasteNode(bool bAfter = true)
        {
            if (!treeView1.Focused)
                return;

            if (treeView1.SelectedNode == null || treeView1.SelectedNode.Text == "testmodule" || BufferXmlNode == null)
                return;

            XmlNode xmlnode = xmlNodeFromTreeNode(treeView1.SelectedNode, XmlDoc);
            XmlNode xmlnodeParent = xmlnode.ParentNode;
            if(bAfter)
            {
                xmlnodeParent.InsertAfter(BufferXmlNode, xmlnode);
                int idx = treeView1.SelectedNode.Index;
                treeView1.SelectedNode.Parent.Nodes.Insert(idx+1, BufferXmlNode.Attributes["title"].Value);
            }   
            else
            {
                xmlnodeParent.InsertBefore(BufferXmlNode, xmlnode);
                int idx = treeView1.SelectedNode.Index;
                treeView1.SelectedNode.Parent.Nodes.Insert(idx, BufferXmlNode.Attributes["title"].Value);
            }

            copyNode();
        }

        public TreeNode getNodeFromPath(TreeNode node, string path)
        {
            TreeNode foundNode = null;
            foreach (TreeNode tn in node.Nodes)
            {
                if (tn.FullPath == path)
                {
                    return tn;
                }
                else if (tn.Nodes.Count > 0)
                {
                    foundNode = getNodeFromPath(tn, path);
                }
                if (foundNode != null)
                    return foundNode;
            }
            return null;
        }

        private void copyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            copyNode();
        }

        private void cutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            cutNode();
        }

        private void pasteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            pasteNode();
        }

        private void pasteBeforeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            pasteNode(false);
        }


        private void openFile()
        {
            //var fileContent = string.Empty;
            //var filePath = string.Empty;

            using (OpenFileDialog openFileDialog = new OpenFileDialog())
            {
                //openFileDialog.InitialDirectory = "c:\\";
                openFileDialog.Filter = "xml files (*.xml)|*.xml|All files (*.*)|*.*";
                openFileDialog.FilterIndex = 1;
                openFileDialog.RestoreDirectory = true;

                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    //Get the path of specified file
                    //filePath = openFileDialog.FileName;
                    FilePath = openFileDialog.FileName;
                    XmlDoc.Load(FilePath);
                    updateTreeview();

                    //Read the contents of the file into a stream
                    //var fileStream = openFileDialog.OpenFile();

                    //using (StreamReader reader = new StreamReader(fileStream))
                    //{
                    //    fileContent = reader.ReadToEnd();
                    //}
                }
            }

            //MessageBox.Show(fileContent, "File Content at path: " + filePath, MessageBoxButtons.OK);
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            openFile();
        }

        private void saveFile()
        {
            if (FilePath != "")
            {
                XmlDoc.Save(FilePath);
            }
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            saveFile();
        }

        private void saveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {   
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();

            saveFileDialog1.Filter = "xml files (*.xml)|*.xml|All files (*.*)|*.*";
            saveFileDialog1.FilterIndex = 1;
            saveFileDialog1.RestoreDirectory = true;

            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                string filePath = saveFileDialog1.FileName.ToString();
                XmlDoc.Save(filePath);
                FilePath = filePath;
            }
        }
        
        private void makeNewXmlNode(string strTitle, string strTestFunc = "", string strParams = "")
        {
            // testgroup
            if(strTestFunc == "" && strParams == "")
            {   
                SelectedXmlNode.Attributes.Append(makeXmlAttr("title", strTitle));
                SelectedTreeNode.Text = strTitle;
                //updateTreeview();
                //treeView1.SelectedNode = treeNodeFromXmlNode(SelectedXmlNode);
            }
            else
            {
                XmlNode nodeTestFunc = SelectedXmlNode.ChildNodes[0];
                XmlNodeList paramlist = nodeTestFunc.ChildNodes;
                
                XmlNode newTestCaseNode = XmlDoc.CreateNode("element", SelectedXmlNode.Name, null);
                newTestCaseNode.Attributes.Append(makeXmlAttr("title", strTitle));
                newTestCaseNode.Attributes.Append(makeXmlAttr("ident", ""));

                string strTestFuncType = "capltestfunction";
                string strTestFuncParam = "caplparam";
                if(netToolStripMenuItem.Checked)
                {
                    strTestFuncType = "nettestfunction";
                    strTestFuncParam = "netparam";
                }

                XmlNode newFuncNode = XmlDoc.CreateNode("element", strTestFuncType, null);
                newFuncNode.Attributes.Append(makeXmlAttr("title", strTitle));
                newFuncNode.Attributes.Append(makeXmlAttr("name", strTestFunc));

                string strParams2 = strParams;
                strParams2 = strParams2.Replace("\r", "");
                string[] strSplitLines = strParams2.Split('\n');
                for(int i=0; i<strSplitLines.Length; i++)
                {
                    string strLine = strSplitLines[i];
                    strLine = strLine.Replace('=', ' ');
                    string[] strSplitSpace = strLine.Split(' ');

                    if(strSplitSpace.Length < 3)
                    {
                        MessageBox.Show("invdalid parameter format");
                        return;
                    }

                    XmlNode newParamNode = XmlDoc.CreateNode("element", strTestFuncParam, null);
                    newParamNode.Attributes.Append(makeXmlAttr("type", strSplitSpace[0]));
                    newParamNode.Attributes.Append(makeXmlAttr("name", strSplitSpace[1]));
                    newParamNode.InnerText = strSplitSpace[2];

                    newFuncNode.AppendChild(newParamNode);
                }
                newTestCaseNode.AppendChild(newFuncNode);
                
                SelectedXmlNode.ParentNode.ReplaceChild(newTestCaseNode, SelectedXmlNode);
                SelectedXmlNode = newTestCaseNode;
                
                //updateTreeview();
                //treeView1.SelectedNode = treeNodeFromXmlNode(SelectedXmlNode);
                SelectedTreeNode.Text = SelectedXmlNode.Attributes["title"].Value;
            }
        }

        private void dataGridView1_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (dataGridView1.Rows[e.RowIndex].Cells[e.ColumnIndex].Value != null)
            {
                if (SelectedXmlNode.Name == "testcase")
                {
                    makeNewXmlNode(
                        dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString(),
                        dataGridView1.Rows[e.RowIndex].Cells[1].Value.ToString(),
                        dataGridView1.Rows[e.RowIndex].Cells[2].Value.ToString());
                }
                else
                {
                   makeNewXmlNode(
                   dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString());
                }
            }
        }

        private void dataGridView1_ColumnWidthChanged(object sender, DataGridViewColumnEventArgs e)
        {
            if (SelectedXmlNode != null)
            {
                ColumnWidth[e.Column.Index] = dataGridView1.Columns[e.Column.Index].Width;
            }
        }

        protected override bool ProcessCmdKey(ref System.Windows.Forms.Message msg, Keys keyData)
        {   
            Keys key = keyData & ~(Keys.Shift | Keys.Control);
            
            if(treeView1.Focused)
            {
                switch (key)
                {
                    case Keys.Delete:
                        delNode();
                        break;
                    case Keys.C:
                        if ((keyData & Keys.Control) != 0)
                            copyNode();
                        break;
                    case Keys.X:
                        if ((keyData & Keys.Control) != 0)
                            cutNode();
                        break;
                    case Keys.V:
                        if (((keyData & Keys.Control) != 0) && ((keyData & Keys.Shift) != 0))
                            pasteNode(false);
                        else if ((keyData & Keys.Control) != 0)
                            pasteNode();
                        break;
                    case Keys.G:
                        if (((keyData & Keys.Control) != 0) && ((keyData & Keys.Shift) != 0))
                            newTestGroupNodeSub();
                        else if ((keyData & Keys.Control) != 0)
                            newTestGroupNode();
                        break;
                    case Keys.N:
                        if (((keyData & Keys.Control) != 0) && ((keyData & Keys.Shift) != 0))
                            newTestCaseNodeSub();
                        else if ((keyData & Keys.Control) != 0)
                            newTestCaseNode();
                        break;
                    default:
                        break;
                }
            }

            // Global shortcut
            switch (key)
            {
                case Keys.S:
                    if ((keyData & Keys.Control) != 0)
                        saveFile();
                    break;
                case Keys.O:
                    if ((keyData & Keys.Control) != 0)
                        openFile();
                    break;
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }

        private void insertTestGroupToolStripMenuItem_Click(object sender, EventArgs e)
        {
            newTestGroupNode();
        }

        private void insertTestGroupSubToolStripMenuItem_Click(object sender, EventArgs e)
        {
            newTestGroupNodeSub();
        }

        private void insertTestStepToolStripMenuItem_Click(object sender, EventArgs e)
        {
            newTestCaseNode();
        }

        private void insertTestStepSubToolStripMenuItem_Click(object sender, EventArgs e)
        {
            newTestCaseNodeSub();
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            About aboutDig = new About();

            Point p = new Point();
            p.X = this.Location.X + this.Width / 2 - aboutDig.Width / 2;
            p.Y = this.Location.Y + this.Height / 2 - aboutDig.Height / 2;


            aboutDig.StartPosition = FormStartPosition.Manual;
            aboutDig.Location = p;
            aboutDig.ShowDialog();
            
        }

        private void Form1_Shown(object sender, EventArgs e)
        {
            if(false == licCheck())
            {
                Close();
            }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }


        private void netToolStripMenuItem_Click(object sender, EventArgs e)
        {
            netToolStripMenuItem.Checked = !netToolStripMenuItem.Checked;
        }
    }
}
